package com.fernandoherrera.usersapp

import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.room.Room
import com.fernandoherrera.usersapp.data.local.UserDatabase
import com.fernandoherrera.usersapp.data.remote.UserApi
import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.domain.repositories.UserRepository
import com.fernandoherrera.usersapp.ui.detail.DetailScreen
import com.fernandoherrera.usersapp.ui.homeScreen.HomeScreen
import com.fernandoherrera.usersapp.ui.homeScreen.HomeViewModel
import com.google.gson.Gson
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val db = Room.databaseBuilder(this, UserDatabase::class.java, "users_db").build()
        val dao = db.dao
        val repository = UserRepository(UserApi.instance, dao)
        val viewModel = HomeViewModel(repository)

        setContent {
            val navController = rememberNavController()
            val actions = remember(navController) { AppActions(navController) }

            NavHost(navController = navController, startDestination = "home") {
                composable(route = "home") {

                    HomeScreen(viewModel) {
                        val userEncodeJson = Uri.encode(Json.encodeToJsonElement(it).toString())
                        navController.navigate("detail/${userEncodeJson}")
                    }
                }
                composable(
                    route = "detail/{userJson}"
                ) {
                    val userJson = Uri.decode(it.arguments?.getString(("userJson")))
                    val user = Gson().fromJson(userJson, UserCharacter::class.java)
                    DetailScreen(user, actions.navigateUp)
                }
            }
        }
    }
}

class AppActions(
    navController: NavHostController
) {
    val navigateUp: () -> Unit = {
        navController.navigateUp()
    }
}
