package com.fernandoherrera.usersapp.data.local

import androidx.room.TypeConverter
import com.fernandoherrera.usersapp.data.remote.dto.Employment
import com.google.gson.Gson

class EmploymentConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromEmployment(employment: Employment): String {
        return gson.toJson(employment)
    }

    @TypeConverter
    fun toEmployment(json: String): Employment {
        return gson.fromJson(json, Employment::class.java)
    }
}
