package com.fernandoherrera.usersapp.data.local

import androidx.room.TypeConverter
import com.fernandoherrera.usersapp.data.remote.dto.Coordinates
import com.fernandoherrera.usersapp.data.remote.dto.Subscription
import com.google.gson.Gson

class SubscriptionConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromSubscription(subscription: Subscription): String {
        return gson.toJson(subscription)
    }

    @TypeConverter
    fun toSubscription(json: String): Subscription {
        return gson.fromJson(json, Subscription::class.java)
    }
}