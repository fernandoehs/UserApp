package com.fernandoherrera.usersapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fernandoherrera.usersapp.data.local.entity.UserCharacterEntity

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacter(characterEntity: UserCharacterEntity)

    @Query("SELECT * FROM UserCharacterEntity")
    suspend fun getCharacters(): List<UserCharacterEntity>
}
