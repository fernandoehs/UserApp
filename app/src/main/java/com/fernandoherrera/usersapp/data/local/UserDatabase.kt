package com.fernandoherrera.usersapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.fernandoherrera.usersapp.data.local.entity.UserCharacterEntity

@Database(entities = [UserCharacterEntity::class], version = 1)
@TypeConverters(EmploymentConverter::class, SubscriptionConverter::class)
abstract class UserDatabase : RoomDatabase() {
    abstract val dao: UserDao
}
