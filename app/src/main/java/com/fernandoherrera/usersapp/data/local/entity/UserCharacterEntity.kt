package com.fernandoherrera.usersapp.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fernandoherrera.usersapp.data.remote.dto.Address
import com.fernandoherrera.usersapp.data.remote.dto.Coordinates
import com.fernandoherrera.usersapp.data.remote.dto.CreditCard
import com.fernandoherrera.usersapp.data.remote.dto.Employment
import com.fernandoherrera.usersapp.data.remote.dto.Subscription

@Entity
data class UserCharacterEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val avatar: String,
    val date_of_birth: String,
    val email: String,
    val first_name: String,
    val gender: String,
    val last_name: String,
    val phone_number: String,
    val social_insurance_number: String,
    val uid: String,
    val username: String,
    val employment: Employment,
    val subscription: Subscription
    )

