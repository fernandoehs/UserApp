package com.fernandoherrera.usersapp.data.mapper

import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.data.local.entity.UserCharacterEntity
import com.fernandoherrera.usersapp.data.remote.dto.DataDtoItem

fun DataDtoItem.toDomain(): UserCharacter? {

    return UserCharacter(
        avatar = this.avatar,
        date_of_birth = this.date_of_birth,
        email = this.email,
        first_name = this.first_name,
        gender = this.gender,
        id = this.id,
        last_name = this.last_name,
        phone_number = this.phone_number,
        social_insurance_number = this.social_insurance_number,
        uid = this.uid,
        username = this.username,
        employment = this.employment,
        subscription = this.subscription
    )

}

fun UserCharacter.toEntity(): UserCharacterEntity {
    return UserCharacterEntity(
        avatar = this.avatar,
        date_of_birth = this.date_of_birth,
        email = this.email,
        first_name = this.first_name,
        gender = this.gender,
        id = this.id,
        last_name = this.last_name,
        phone_number = this.phone_number,
        social_insurance_number = this.social_insurance_number,
        uid = this.uid,
        username = this.username,
        employment = this.employment,
        subscription = this.subscription
    )

}

fun UserCharacterEntity.toDomain(): UserCharacter {
    return UserCharacter(
        avatar = this.avatar,
        date_of_birth = this.date_of_birth,
        email = this.email,
        first_name = this.first_name,
        gender = this.gender,
        id = this.id,
        last_name = this.last_name,
        phone_number = this.phone_number,
        social_insurance_number = this.social_insurance_number,
        uid = this.uid,
        username = this.username,
        employment = this.employment,
        subscription = this.subscription

    )

}