package com.fernandoherrera.usersapp.data.remote

import com.fernandoherrera.usersapp.data.remote.dto.DataDtoItem
import com.fernandoherrera.usersapp.util.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {
    companion object {
        val instance = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(OkHttpClient.Builder().build()).build().create(UserApi::class.java)
    }

    @GET("users")
    suspend fun getCharacters(@Query("size") size: Int): List<DataDtoItem>
}
