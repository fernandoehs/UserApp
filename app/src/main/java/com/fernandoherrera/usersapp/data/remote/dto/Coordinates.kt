package com.fernandoherrera.usersapp.data.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class Coordinates(
    val lat: Double,
    val lng: Double
)