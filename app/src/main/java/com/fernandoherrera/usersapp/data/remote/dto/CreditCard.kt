package com.fernandoherrera.usersapp.data.remote.dto

data class CreditCard(
    val cc_number: String
)