package com.fernandoherrera.usersapp.data.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class Employment(
    val key_skill: String,
    val title: String
)