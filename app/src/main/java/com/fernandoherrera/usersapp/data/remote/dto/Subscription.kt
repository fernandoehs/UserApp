package com.fernandoherrera.usersapp.data.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class Subscription(
    val payment_method: String,
    val plan: String,
    val status: String,
    val term: String
)