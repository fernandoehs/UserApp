package com.fernandoherrera.usersapp.domain

import com.fernandoherrera.usersapp.data.remote.dto.Employment
import com.fernandoherrera.usersapp.data.remote.dto.Subscription
import kotlinx.serialization.Serializable


@Serializable
data class UserCharacter(
    val avatar: String,
    val date_of_birth: String,
    val email: String,
    val first_name: String,
    val gender: String,
    val id: Int,
    val last_name: String,
    val phone_number: String,
    val social_insurance_number: String,
    val uid: String,
    val username: String,
    val employment: Employment,
    val subscription: Subscription
)
