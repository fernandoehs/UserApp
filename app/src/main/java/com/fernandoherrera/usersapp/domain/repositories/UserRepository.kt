package com.fernandoherrera.usersapp.domain.repositories

import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.data.local.UserDao
import com.fernandoherrera.usersapp.data.mapper.toDomain
import com.fernandoherrera.usersapp.data.mapper.toEntity
import com.fernandoherrera.usersapp.data.remote.UserApi

class UserRepository(
    private val api: UserApi,
    private val dao: UserDao
) {

    suspend fun getCharacters(): List<UserCharacter> {
        var localCharacters = dao.getCharacters()

        if (localCharacters.isEmpty()){   // esta data local vacia?
            val remoteCharacters = getCharactersRemote()
            remoteCharacters.forEach{
                dao.insertCharacter(it.toEntity())
            }
            localCharacters = dao.getCharacters()
        }
        return localCharacters.map{it.toDomain()}
    }

    private suspend fun getCharactersRemote(): List<UserCharacter> {
        return try {
            val characters = api.getCharacters(10)
            characters.mapNotNull{it.toDomain()}
        }catch (e:Exception) {
            emptyList()
        }
    }


}
