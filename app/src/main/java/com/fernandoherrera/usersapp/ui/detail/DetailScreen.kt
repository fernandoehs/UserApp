package com.fernandoherrera.usersapp.ui.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.EmojiPeople
import androidx.compose.material.icons.outlined.DateRange
import androidx.compose.material.icons.outlined.Email
import androidx.compose.material.icons.outlined.Handyman
import androidx.compose.material.icons.outlined.Help
import androidx.compose.material.icons.outlined.Numbers
import androidx.compose.material.icons.outlined.Phone
import androidx.compose.material.icons.outlined.SafetyDivider
import androidx.compose.material.icons.outlined.Subscriptions
import androidx.compose.material.icons.outlined.Work
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.fernandoherrera.usersapp.R
import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.ui.detail.components.CharacterImage
import com.fernandoherrera.usersapp.ui.detail.components.DetailProperty
import com.fernandoherrera.usersapp.ui.detail.components.DetailVarious
import com.fernandoherrera.usersapp.ui.detail.components.mirroringBackIcon

@Composable
fun DetailScreen(
    user: UserCharacter,
    upPress: () -> Unit
) {

    DetailContent(
        user = user,
        upPress = upPress,
    )
}

@Composable
private fun DetailContent(
    modifier: Modifier = Modifier,
    user: UserCharacter,
    upPress: () -> Unit
) {
    Box(modifier.fillMaxSize()) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            item {
                Header(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(240.dp),
                    character = user,
                )
            }
            item {
                Body(character = user)
            }
    }

        Up(upPress)
    }
}

@Composable
private fun Header(
    modifier: Modifier = Modifier,
    character: UserCharacter
) {
    Column(
        modifier = modifier.background(Color(0xffffe0b2)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CharacterImage(image = character.avatar)
        Spacer(modifier = Modifier.height(12.dp))

        Column {
            TwoTexts(firstText = character.first_name, secondText = character.last_name)
        }
    }
}

@Composable
private fun Body(character: UserCharacter?) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        DetailProperty(label = stringResource(R.string.username), value = character?.username, imageVector = Icons.Filled.EmojiPeople)
        DetailProperty(label = stringResource(R.string.phone), value = character?.phone_number, imageVector = Icons.Outlined.Phone)
        DetailProperty(label = stringResource(R.string.email), value = character?.email, imageVector = Icons.Outlined.Email)
        DetailProperty(label = stringResource(R.string.dateBirth), value = character?.date_of_birth, imageVector = Icons.Outlined.DateRange)
        DetailProperty(label = stringResource(R.string.gender), value = character?.gender, imageVector = Icons.Outlined.SafetyDivider)
        DetailProperty(label = stringResource(R.string.socialNumber), value = character?.social_insurance_number, imageVector = Icons.Outlined.Numbers)
        DetailProperty(label = stringResource(R.string.employment), value = character?.employment?.title, imageVector = Icons.Outlined.Work)
        DetailProperty(label = stringResource(R.string.employmentSkill), value = character?.employment?.key_skill, imageVector = Icons.Outlined.Handyman)
        if (character != null) {
            DetailVarious(label = stringResource(R.string.subscription),
                value = character, imageVector = Icons.Outlined.Subscriptions)
        }
    }
}
@Composable
fun TwoTexts(firstText: String, secondText: String) {
    Row {
        Text(
            text = firstText,
            style = MaterialTheme.typography.h5,
            color = Color.Black,
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(
            text = secondText,
            style = MaterialTheme.typography.h5,
            color = Color.Black
        )
    }
}
@Composable
private fun Up(upPress: () -> Unit) {
    IconButton(
        onClick = upPress,
        modifier = Modifier
            .padding(horizontal = 12.dp, vertical = 10.dp)
            .size(36.dp)
    ) {
        Icon(
            imageVector = mirroringBackIcon(),
            tint = Color(0xFF0C0B0B),
            contentDescription = null
        )
    }
}