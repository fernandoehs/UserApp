package com.fernandoherrera.usersapp.ui.detail

import com.fernandoherrera.usersapp.domain.UserCharacter


data class DetailState(
    val character: UserCharacter? = null,
    val isLoading: Boolean = false
)