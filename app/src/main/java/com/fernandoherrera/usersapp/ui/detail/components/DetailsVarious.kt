package com.fernandoherrera.usersapp.ui.detail.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.fernandoherrera.usersapp.R
import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.ui.detail.TwoTexts

@Composable
fun DetailVarious(
    modifier: Modifier = Modifier,
    label: String,
    value: UserCharacter,
    imageVector: ImageVector
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp, horizontal = 4.dp),
        elevation = 3.dp,
        backgroundColor = Color(0xffba2d65)
    ) {
        Row(modifier = Modifier.padding(8.dp)) {
            Icon(
                imageVector = imageVector, 
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
            Spacer(Modifier.width(10.dp))
            Column {
                Text(text = label, fontWeight = FontWeight.Bold, color = Color.White)
                Spacer(Modifier.height(5.dp))
                Column {
                    TwoTextsSubcription(firstText = stringResource(R.string.plan), secondText = value.subscription.plan)
                    TwoTextsSubcription(firstText = stringResource(R.string.statusSubscription), secondText = value.subscription.status)
                    TwoTextsSubcription(firstText = stringResource(R.string.payment_method), secondText = value.subscription.payment_method)
                    TwoTextsSubcription(firstText = stringResource(R.string.term), secondText = value.subscription.term)

                }
            }
        }
    }
}

@Composable
fun TwoTextsSubcription(firstText: String, secondText: String) {
    Row {
        Text(
            text = firstText,
            color = Color.White,
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(
            text = secondText,
            color = Color.White
        )
    }
}
