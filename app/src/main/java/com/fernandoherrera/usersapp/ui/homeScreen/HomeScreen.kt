package com.fernandoherrera.usersapp.ui.homeScreen

import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewModelScope
import com.fernandoherrera.usersapp.R
import com.fernandoherrera.usersapp.domain.UserCharacter
import com.fernandoherrera.usersapp.ui.homeScreen.components.CharacterItem

@Composable
fun HomeScreen(
    viewModel: HomeViewModel= hiltViewModel(),
    onItemClicked: (UserCharacter) -> Unit
) {
    val context = LocalContext.current
    val packageManager: PackageManager = context.packageManager
    val intent: Intent = packageManager.getLaunchIntentForPackage(context.packageName)!!
    val componentName: ComponentName = intent.component!!
    val restartIntent: Intent = Intent.makeRestartActivityTask(componentName)
    val state = viewModel.state
    val showDialog = remember { mutableStateOf(false) }

    if (state.isLoading) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator(
                color = Color.Blue,
                strokeWidth = 8.dp,
                modifier = Modifier.size(100.dp)
            )
        }
    }
    else if(state.error) {
        AlertDialog(
            onDismissRequest = { showDialog.value = false },
            title = { Text(text = stringResource(R.string.error)) },
            text = { Text(text = stringResource(R.string.dont_exist_internet_connection)) },
            confirmButton = {
                Button(
                    onClick = {
                        context.startActivity(restartIntent)
                        Runtime.getRuntime().exit(0)}
                ) {
                    Text(text = "Retry")
                }
            }
        )

        }
    else {
        LazyColumn(modifier = Modifier.fillMaxWidth()) {
            val characterIndices = state.characters.indices.toList()

            items(characterIndices) { index ->
                CharacterItem(
                    modifier = Modifier.fillMaxWidth(),
                    item = state.characters[index],
                    onItemClicked = {onItemClicked(it) }
                )
            }
        }
    }
}