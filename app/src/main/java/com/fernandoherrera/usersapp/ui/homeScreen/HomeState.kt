package com.fernandoherrera.usersapp.ui.homeScreen

import com.fernandoherrera.usersapp.domain.UserCharacter

data class HomeState(
    val characters: List<UserCharacter> = emptyList(),
    val isLoading: Boolean = false,
    val error: Boolean = false

)
