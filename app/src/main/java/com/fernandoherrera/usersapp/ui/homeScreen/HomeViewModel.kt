package com.fernandoherrera.usersapp.ui.homeScreen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fernandoherrera.usersapp.domain.repositories.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: UserRepository
) : ViewModel() {
    var state by mutableStateOf(HomeState())
        private set
    init {
        viewModelScope.launch {
            state= state.copy(
                isLoading = true
            )
            val characters = repository.getCharacters()
            state = if (characters.isEmpty()){
                state.copy(
                    characters = characters,
                    isLoading = false,
                    error = true
                )
            } else{
                state.copy(
                    characters = characters,
                    isLoading = false
                )
            }

        }
    }
}
