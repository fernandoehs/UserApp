package com.fernandoherrera.usersapp.ui.homeScreen.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Circle
import androidx.compose.material.icons.outlined.CircleNotifications
import androidx.compose.material.icons.outlined.CrisisAlert
import androidx.compose.material.icons.outlined.Handyman
import androidx.compose.material.icons.outlined.NextPlan
import androidx.compose.material.icons.outlined.NotificationsActive
import androidx.compose.material.icons.outlined.SmartButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import com.fernandoherrera.usersapp.R
import com.fernandoherrera.usersapp.domain.UserCharacter


@Composable
fun CharacterItem(
    modifier: Modifier = Modifier,
    item: UserCharacter,
    onItemClicked: (UserCharacter) -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp),
        elevation = 5.dp,
        shape = RoundedCornerShape(16.dp),
        border = BorderStroke(2.dp, Color.Black),
        backgroundColor = Color.LightGray
    ) {
        Row(
            modifier = modifier
                .clickable { onItemClicked(item) }
                .padding(start = 6.dp, top = 12.dp, bottom = 12.dp)
        ) {
            CharacterImageContainer(modifier = Modifier.size(64.dp)) {
                CharacterImage(item)
            }
            Spacer(Modifier.width(20.dp))
            Column(
                modifier = Modifier
                    .width(120.dp)
                    .align(Alignment.CenterVertically)
            ) {
                Text(
                    text = item.first_name,
                    style = MaterialTheme.typography.h6
                )
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(
                        text = item.last_name,
                        style = MaterialTheme.typography.caption
                    )
                }

            }
            Spacer(Modifier.width(20.dp))
            Column(
                modifier = Modifier
                    .wrapContentSize()
                    .align(Alignment.Top)

            ) {
                Row {
                    Text(
                        text = stringResource(R.string.statusCard),
                        style = MaterialTheme.typography.caption
                    )
                    Text(
                        text = item.subscription.status,
                        style = MaterialTheme.typography.caption,
                        color = when (item.subscription.status) {
                            "Blocked" -> Color.Red
                            "Idle" -> Color.Blue
                            "Pending" -> Color.Yellow
                            "Active" -> Color.Black
                            else -> Color.LightGray
                        }
                    )
                }
            }
            Divider(modifier = Modifier.padding(top = 10.dp))
        }
    }
}



@Composable
fun CharacterImage(item: UserCharacter) {
    Box {
        val painter = rememberAsyncImagePainter(
            model = ImageRequest.Builder(LocalContext.current)
                .data(item.avatar)
                .size(Size.ORIGINAL)
                .build()
        )
        Image(
            painter = painter,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
    }
}

// This function may be private
@Composable
fun CharacterImageContainer(
    modifier: Modifier,
    content: @Composable () -> Unit
) {
    Surface(modifier.clip(CircleShape) , RoundedCornerShape(4.dp)) {
       content()
    }
}